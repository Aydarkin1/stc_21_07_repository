package com.company;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;
    private String serialNumber;

    private User(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorker = builder.isWorker;
        this.serialNumber = builder.serialNumber;
    }

    public static class Builder {
        private String firstName;
        private String lastName;
        private int age;
        private boolean isWorker;
        private String serialNumber;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            this.isWorker = isWorker;
            return this;

        }

        public Builder serialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
            return this;

        }

        public User build() {
            return new User(this);
        }
    }

    public static Builder builder() {
        return new Builder();
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public Boolean getWorker() {
        return isWorker;
    }

    public String getSerialNumber() {
        return serialNumber;
    }


    @Override
    public String toString() {
        return "User: " + firstName +
                " " + lastName +
                ", Возраст " + age +
                ", Работает " + isWorker +
                ", Cерийный номер " + serialNumber +
                "";
    }
}

}
