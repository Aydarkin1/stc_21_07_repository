package com.company;

public class Main {
    public static void main(String[] args) {

        User user = User.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .age(26)
                .isWorker(true)
                .build();

        System.out.println( "User: " + user.getFirstName() +
                " " + user.getLastName() +
                ", Возраст " + user.getAge() +
                ", Работает " + user.getWorker() + "\n");


        User user2 = User.builder()
                .firstName("Сергей")
                .lastName("Шутов")
                .age(2)
                .isWorker(true)
                .serialNumber("1987")
                .build();

        System.out.println(user2);
    }

}

}
