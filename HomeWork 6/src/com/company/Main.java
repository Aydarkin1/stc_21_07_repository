package com.company;

public class Main {

    public static void main(String[] args) {
        TV tv = new TV(name:"TV Samsung");
        RemoteController control = new RemoteController(tv);
        Program p1 = new Program(name:"Первый");
        Program p2 = new Program(name:"Второй");
        Program p3 = new Program(name:"Третий");
        Program p4 = new Program(name:"Четвертый");
        Program p5 = new Program(name:"Пятый");
        Program p6 = new Program(name:"Шестой");
        Program p7 = new Program(name:"Седьмой");
        Program p8 = new Program(name:"Восьмой");
        Program p9 = new Program(name:"Девятый");
        Program p10 = new Program(name:"Десятый");

        Setters setters = new Setters();
        setters.settingsPrograms(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);

        Printer printer = new Printer();
        printer.printTV(tv);
        printer.printRemoteController(control);
    }
}
