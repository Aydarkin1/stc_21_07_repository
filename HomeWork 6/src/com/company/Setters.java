package com.company;

public class Setters {
    public static void settingsPrograms(Program p1, Program p2, Program p3, Program p4, Program p5, Program p6, Program p7, Program p8, Program p9, Program p10) {
        p1.setName("Первый ");
        p1.setAges(16);
        p2.setName("Второй ");
        p2.setAges(18);

        p3.setName("Третий");
        p3.setAges(12);
        p4.setName("Четвертый");
        p4.setAges(18);
        p5.setName("Пятый");
        p5.setAges(10);
        p6.setName("Шестой");
        p6.setAges(11);
        p7.setName("Седьмой");
        p7.setAges(12);
        p8.setName("Восьмой");
        p8.setAges(13);
        p9.setName("Девятый");
        p9.setAges(14);
        p10.setName("Десятый");
        p10.setAges(15);

    }
}

