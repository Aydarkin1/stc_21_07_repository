package com.company;

public class RemoteController {
    private TV tv;

    public RemoteController(TV tv) {
        this.tv = tv;
    }

    public void turn(int channelNumber) {
        tv.showChannel(channelNumber);
    }
}


