package com.company;

public class TV {
    private static final int MAX_CHANNELS_COUNT = 10;
    private String name;
    private Channel[] channels;
    private int channelsCount;

    public TV(String name) {
        this.name = name;
        this.channelsCount = 0;
        this.channels = new Channel[MAX_CHANNELS_COUNT];
    }

    public void addChannel(Channel channel) {
        if (channelsCount < MAX_CHANNELS_COUNT) {
            channels[channelsCount] = channel;
            channelsCount++;
        }
    }

    public void showChannel(int channelNumber) {
        if (channelNumber < channelsCount && channelNumber >= 0) {
            channels[channelNumber].showProgram();
        } else {
            System.out.println("\u001B[31mНастройте канал! \u001B[0m");
        }
    }

    public String getName() {
        return name;
    }
}
