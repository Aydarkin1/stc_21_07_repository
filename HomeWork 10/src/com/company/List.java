package com.company;

public interface List extends Collection {

    int get(int index);
    void addFirst(int element);
    void removeByIndex(int index);
    int indexOf(int element);
}
