package com.company;


public class LinkedList implements List {

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    // ссылка на начало списка, на первый узел
    private Node first;
    // ссылка на последний элемент списка, на последний узел
    private Node last;

    private int size;

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;

            for (int i = 0; i < index; i++) {
                // отсчитываете index-штук узлов до нужного
                current = current.next;
            }
            // возвращаете его значение
            return current.value;
        }
        System.err.println("Index out of bounds");
        return -1;
    }

    @Override
    public void addFirst(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            add(element);
        } else {
            // если уже есть элементы в списке
            // следующий узел после нового - это первый узел списка
            newNode.next = first;
            // теперь новый узел - первый в списке
            first = newNode;
            size++;
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        // если в списке еще нет элементов
        if (first == null) {
            // то первый и последний элемент - это новый
            first = newNode;
            last = newNode;
        } else {
            // теперь последний элемент списка ссылается на новый, следовательно новый встал в конец списка
            last.next = newNode;
            // теперь новый узел является последним
            last = newNode;
        }
        size++;
    }

    @Override
    public boolean contains(int element) {
        Node current = first;
        for (int i = 0; i < size; i++) {
            if (current.value == element) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    public int indexOf(int element) {
        int i = 0;
        Node current;

        for (current = first; current != null && current.value != element; current = current.next) {
            i++;
        }
        if (current != null) {
            return i;
        }
        else {
            return -1;
        }
    }

    @Override
    public void remove(int element) {
        removeByIndex(indexOf(element));
    }

    @Override
    public void removeByIndex(int index) {
        Node current = first;
        Node currentNext = current.next;

        if (index > 0 && index < size) {
            for (int i = 0; i < index - 1; i++) {
                current = current.next;
                currentNext = current.next;
            }
            current.next = currentNext.next;
            size--;
        }
        else if (index == 0) {
            first = current.next;
            size--;
        }
    }

    private class LinkedListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int value = get(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < size;
        }
    }

    @Override
    public Iterator iterator() {
        return new LinkedList.LinkedListIterator();
    }
}