package com.company;


public class Main {

    public static void main(String[] args) {

        Calculation calc = new Calculation();
        List listArray = new ArrayList();
        listArray.add(1);
        listArray.add(2);
        listArray.add(3);
        listArray.add(4);
        listArray.add(5);
        listArray.add(6);

        calc.ListArrayPrint(listArray);


        List listLinked = new LinkedList();
        listLinked.add(1);
        listLinked.add(2);
        listLinked.add(3);
        listLinked.add(4);
        listLinked.add(5);

        calc.LinkedListPrint(listLinked);
    }
}