package com.company;

public class Calculation {

    public static void ListArrayPrint(List listArray) {

        listArray.addFirst(777);
        listArray.remove(4);
        listArray.removeByIndex(1);

        Iterator iteratorListArray = listArray.iterator();

        while (iteratorListArray.hasNext()) {
            System.out.print(+iteratorListArray.next() + " ");
        }

        System.out.println("\nПроверка эелемента 3 is " + listArray.contains(3));
        System.out.println("Размер " + listArray.size());
        System.out.println("_____________________________");
    }

    public static void LinkedListPrint(List listLinked) {

        listLinked.addFirst(777);
        listLinked.remove(4);
        listLinked.removeByIndex(2);

        Iterator iteratorListLinked = listLinked.iterator();

        while (iteratorListLinked.hasNext()) {
            System.out.print(+iteratorListLinked.next() + " ");
        }

        System.out.println("\nПроверка эелемента 2 is " + listLinked.contains(2));
        System.out.println("Размер " + listLinked.size());
    }

}
