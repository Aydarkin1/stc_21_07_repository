package com.company;

public interface Iterator {
    /**
     * перейти к следующему элементу
     * @return элемент
     */
    int next();

    /**
     * проверить, есть ли следующий элемент
     * @return true если следующий элемент есть, false - если следующего элемента нет
     */
    boolean hasNext();
}
