import java.util.Arrays;
import java.util.Scanner;

public class Program4 {
    static int[] returnReversArray(int[] arr) {
        int j = 0;
        int[] res = new int[arr.length]; //Создаём временный массивчик
        for (int i = arr.length - 1; i >= 0; i--, j++) {
            res[j] = arr[i];
            System.out.print(res[j] + " ");
        }
        return res;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите размерность массива: ");
        int sizeArr = in.nextInt();

        int[] arr = new int[sizeArr];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 20 - 5);
        }

        System.out.print("Первоначальные элементы массива: ");
        for (int key : arr) {
            System.out.print(key + " ");
        }

        System.out.print("\nПереработонные элементы массива: ");
        int[] arr2 = returnReversArray(arr);

        System.out.print("\nВозвратили и обработали массив: ");
        for (int key : arr2) {
            System.out.print(key + " ");
        }

        System.out.println("\n" + Arrays.toString(arr2));
    }
}
//моя программа