package com.company;

public class Main {
    public static void main(String[] args) {
        int[] number = {38788, 23763, 72342};
        String[] string = {"Программа", "Магазин", "Киоск"};

        NumbersAndStringProcessor nasp = new NumbersAndStringProcessor(number, string);
        Calculation calculation = new Calculation();
        calculation.calc(nasp);

    }

}

