package com.company;

import java.util.Arrays;

public class Calculation {

    public static void calc(com.company.NumbersAndStringProcessor nasp) {

        com.company.NumbersProcess Digits = number -> {
            int result;

            result = number;

            return result;
        };

        com.company.StringsProcess String = str -> {
            String result = "";

            result = str;

            return result;
        };

        com.company.NumbersProcess reverseDigits = number -> {
            int result;

            for (result = 0; number != 0; number /= 10) {
                result = (result * 10) + (number % 10);
            }

            return result;
        };


        com.company.NumbersProcess delInDigitsZero = number -> {
            int reverse;
            int result;
            int place = 1;

            for (result = 0; number > 0; number = number / 10) {
                reverse = number % 10;
                if (reverse != 0) {
                    result = result + (reverse * place);
                    place = place * 10;
                }
            }
            return result;
        };


        com.company.NumbersProcess replaceOddNumbers = number -> {
            int result;
            int place = 1;

            for (result = 0; number > 0; number = number / 10) {
                if (number % 2 == 1) {
                    number -= 1;
                }
                result = result + ((number % 10) * place);
                place = place * 10;
            }
            return result;
        };


        com.company.StringsProcess reverseString = str -> {
            String result = "";
            for (int i = 0; i < str.length(); i++) {
                result = str.charAt(i) + result;
            }
            return result;
        };


        com.company.StringsProcess delDigitString = str -> {
            String result = "";
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isDigit(str.charAt(i)))
                    result += str.charAt(i);
            }
            return result;
        };


        com.companyя.StringsProcess upperString = str -> {
            String result = "";
            for (int i = 0; i < str.length(); i++) {
                if (Character.isLetterOrDigit(str.charAt(i)))
                    result += Character.toUpperCase(str.charAt(i));

            }
            return result;
        };


        System.out.println("Исходные числа " + Arrays.toString(nasp.process(Digits)) +
                " ; " + "Исходные строки " + Arrays.toString(nasp.process(String)) +
                "\n\nИнверсия чисел " + Arrays.toString(nasp.process(reverseDigits)) +
                "\nУбрать нули из исходных чисел " + Arrays.toString(nasp.process(delInDigitsZero)) +
                "\nЗаменить нечетные цифры ближайшей четной снизу " + Arrays.toString(nasp.process(replaceOddNumbers)) +
                "\n\nИнверсия строк " + Arrays.toString(nasp.process(reverseString)) +
                "\nУбрать все цифры из строк " + Arrays.toString(nasp.process(delDigitString)) +
                "\nСделать все маленькие буквы большими " + Arrays.toString(nasp.process(upperString)));
    }
}
