package com.company;

public class NumbersAndStringProcessor {
    private int[] number;
    private String[] string;

    public NumbersAndStringProcessor(int[] number, String[] string) {
        this.number = number;
        this.string = string;
    }

    public int[] process(com.company.NumbersProcess process) {
        int[] numbers = new int[number.length];
        for (int i = 0; i < number.length; i++) {
            numbers[i] = process.process(number[i]);
        }

        return numbers;
    }

    public String[] process(StringsProcess process) {
        String[] strings = new String[string.length];
        for (int i = 0; i < string.length; i++) {
            strings[i] = process.process(string[i]);
        }

        return strings;
    }

}
