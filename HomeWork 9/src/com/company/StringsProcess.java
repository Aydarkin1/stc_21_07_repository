package com.company;

public interface StringsProcess {
    String process(String string);
}
