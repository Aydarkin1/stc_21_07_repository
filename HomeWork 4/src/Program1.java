public class Program1 {
    public int binarySearch(List<Integer> list, int find) {
        int mid = list.size() / 2;
        int val = list.get(mid);

        if (val == find) {
            return val;
        } else if (list.size() == 1) {
            return -1;
        }

        return val > find ?
                binarySearch(list.subList(0, mid), find) :
                binarySearch(list.subList(mid, list.size()), find);
    }

    public class List<T> {
        public int get(T mid) {
            return 0;
        }

        public int size() {
            return 0;
        }

        public List<T> subList(T mid, T size) {
            return null;
        }
    }
}
