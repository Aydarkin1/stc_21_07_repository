package com.company;

public class PutMap {

    public static Map<String, Integer> putMap() {
        Map<String, Integer> map = new HashMap<>();

        map.put("Марсель", 17);
        map.put("Айдар", 19);
        map.put("Денис", 20);
        map.put("Даниил", 22);
        map.put("Ильгам", 21);
        map.put("Альберт", 24);
        map.put("Василий", 23);
        map.put("Анатолий", 25);
        map.put("Аня", 26);
        return map;
    }
}
