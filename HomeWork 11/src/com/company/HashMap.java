package com.company;

public class HashMap<K, V> implements Map<K, V> {
    private static final int MAX_TABLE_SIZE = 8;
    private MapEntry<K, V>[] table;

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    public HashMap() {
        this.table = new MapEntry[MAX_TABLE_SIZE];
    }

    @Override
    public void put(K key, V value) {
        int hash = key.hashCode();
        int index = hash & (table.length - 1);

        if (table[index] == null) {
            table[index] = new MapEntry<>(key, value);
        }
        else {
            MapEntry<K, V> current = table[index];
            while (current != null) {
                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                }
                current = current.next;
            }
            MapEntry<K, V> newEntry = new MapEntry<>(key, value);
            newEntry.next = table[index];
            table[index] = newEntry;
        }
    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (table.length - 1);
        MapEntry<K, V> current = table[index];
        while (current != null) {
            if (current.key.equals(key)) {
                return current.value;
            }
            current = current.next;
        }
        System.out.print("Не верный ключ: ");
        return null;
    }
}
