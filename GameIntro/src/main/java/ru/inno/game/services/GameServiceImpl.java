package ru.inno.game.services;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repositories.GamesRepository;
import ru.inno.game.repositories.PlayersRepository;
import ru.inno.game.repositories.ShotsRepository;

import java.time.LocalDateTime;
import java.util.Optional;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
       System.out.println(firstIp + "" + secondIp + "" + firstPlayerNickname + "" + secondPlayerNickname + "");
        // получили первого игрока
       //Player first = checkIxExists(firstIp, firstPlayerNickname);
       //// получили второго игрока
       //Player second = checkIxExists(secondIp, secondPlayerNickname);
       //// создаем игру
       //Game game = Game.builder()
       //        .playerFirst(first)
       ///        .playerSecond(second)
        //       .secondsGameTimeAmount(0L)
        //       .playerFirstShotsCount(0)
        //       .playerSecondShotsCount(0)
        //       .dateTime(LocalDateTime.now())
        //       .build();
        //amesRepository.save(game);
        //eturn game.getId();
        return null;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем информацию об игроках
        // TODO: сделать проверку на isPresent
        Player shooter = playersRepository.findByNickname(shooterNickname).get();
        Player target = playersRepository.findByNickname(targetNickname).get();
        // находим игру
        Game game = gamesRepository.getById(gameId);
        // создаем выстрел
        Shot shot = Shot.builder()
                .shooter(shooter)
                .target(target)
                .dateTime(LocalDateTime.now())
                .game(game)
                .build();
        // увеличили очки у того, кто стрелял
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелял первый игрок
        // TODO: обновление информации по игре в сервисе
//        if (game.getPlayerFirst().getId().equals(shooter.getId())) {
//            // увеличиваем количество попаданий в этой игре
//            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
//        }
//        // если стрелял второй игрок
//        if (game.getPlayerSecond().getId().equals(shooter.getId())) {
//            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
//        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }

    // ищет игрока по никнейму, если такой игрок был - она меняет его IP
    // если игрока не было, она создает нового и сохраняет его
    private Player checkIxExists(String ip, String nickname) {
        Player result;

        Optional<Player> playerOptional = playersRepository.findByNickname(nickname);
        // если игрока под таким именем нет
        if (!playerOptional.isPresent()) {
            // создаем игрока
            Player player = Player.builder()
                    .ip(ip)
                    .nickname(nickname)
                    .losesCount(0)
                    .winsCount(0)
                    .points(0)
                    .build();
            // сохраняем его в репозитории
            playersRepository.save(player);
            result = player;
        } else {
            Player player = playerOptional.get();
            player.setIp(ip);
            playersRepository.update(player);
            result = player;
        }

        return result;
    }
}
