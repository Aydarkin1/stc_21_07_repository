package ru.inno.game.repositories;

import ru.inno.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into game(datetime, player_first, player_second, player_first_shots_count, player_second_shots_count, second_game_time_amount) values (?, ?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from game where id = ?";

    //language=SQL
    private final static String SQL_UPDATE = "update game set " + "dateTime = ?, " + "player_first = ?, " + "player_second = ?, " + "player_first_shots_count = ?, " +
            "player_second_shots_count = ?, " + "second_game_time_amount = ? " + "where id=?";

    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, shot.getDateTime().toString());
            statement.setInt(3, shot.getGame().getId());
            statement.setInt(4, Math.toIntExact(shot.getShooter().getId()));
            statement.setInt(5, Math.toIntExact(shot.getTarget().getId()));
            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot save shot");
            }

            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                int generatedIdIn = keys.getInt("id");
                shot.setId((long) generatedIdIn);
            } else {
                throw new SQLException("Cannot return id");
            }
            keys.close();


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
