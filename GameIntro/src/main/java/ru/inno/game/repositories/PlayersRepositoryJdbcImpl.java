package ru.inno.game.repositories;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;


public class PlayersRepositoryJdbcImpl implements PlayersRepository {
    //language=SQL
    private final static String SQL_FIND_BY_NICKNAME = "select * from player where nickname = ?";

    //language=SQL
    private final static String SQL_SELECT = "select name from player where nickname = ?";

    //language=SQL
    private final static String SQL_INSERT = "insert into player(ip, nickname, points, max_wins_count, max_loses_count) values (?, ?, ?, ?, ?)";

    //language=SQL
    private final static String SQL_UPDATE = "update player set " + "ip = ?, " + "nickname = ?, " + "points = ?, " + "max_wins_count = ?, " +
            "max_loses_count = ? " + "where id=?";

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Player> findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            statement.setString(1, nickname);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return Optional.of(Player.builder()
                            .id(result.getInt("id"))
                            .nickname(result.getString("name"))
                            .ip(result.getString("ip"))
                            .points(result.getInt("points"))
                            .build());
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getNickname());
            statement.setString(2, player.getIp());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert player");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                player.setId(generatedKeys.getInt("id"));
            } else {
                throw new SQLException("Can't obtain id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getNickname());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getWinsCount());
            statement.setInt(5, player.getLosesCount());
            statement.setInt(6, player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update player");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}


