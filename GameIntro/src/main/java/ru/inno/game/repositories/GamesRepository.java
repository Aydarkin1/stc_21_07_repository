package ru.inno.game.repositories;

import ru.inno.game.models.Game;

public interface GamesRepository {
    void save(Game game);

    void update(Game game);

    Game findById(Integer id);

    Game getById(Long gameId);
}

