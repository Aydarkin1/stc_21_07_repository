package ru.inno.game.repositories;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Optional;


public abstract class GamesRepositoryJdbcImpl implements GamesRepository {

    private static final String SQL_INSERT =
            "insert into game (date, first_player, second_player, fist_player_shots_count," +
                    " second_player_shots_count, duration_game) values (?, ?, ?, ?, ?, ?);";

    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";

    private static final String SQL_UPDATE = "update game set " + "date = ?, " + "first_player = ?, " +
            "second_player = ?, " + "fist_player_shots_count = ?, " + "second_player_shots_count = ?, " +
            "duration_game = ? " + "where id = ?";

    private final DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getGameDate().toString());
            statement.setInt(2, game.getFirstPlayer().getId());
            statement.setInt(3, game.getSecondPlayer().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getDurationGame());
            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot save game");
            }

            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                int generatedId = keys.getInt("id");
                game.setId(generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            keys.close();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Integer id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
                Optional<Player> playerFirst = playersRepository.findByNickname(result.getString("first_player"));
                Optional<Player> playerSecond = playersRepository.findByNickname(result.getString("second_player"));

                return Game.builder()
                        .id(result.getInt("id"))
                        .gameDate(LocalDateTime.parse(result.getString("date")))
                        .firstPlayer(Player.builder().id(result.getInt("player_first")).build())
                        .secondPlayer(Player.builder().id(result.getInt("player_first")).build())
                        .playerFirstShotsCount(result.getInt("shot_count_player_one"))
                        .playerSecondShotsCount(result.getInt("shot_count_player_two"))
                        .durationGame(result.getLong("game_duration"))
                        .build();

            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, game.getGameDate().toString());
            statement.setInt(2, game.getFirstPlayer().getId());
            statement.setInt(3, game.getSecondPlayer().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getDurationGame());
            statement.setInt(7, game.getId());

            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot update game");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
