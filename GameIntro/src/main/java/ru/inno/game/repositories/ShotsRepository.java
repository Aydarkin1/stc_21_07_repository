package ru.inno.game.repositories;

import ru.inno.game.models.Shot;

public interface ShotsRepository {

    void save(Shot shot);
}
