package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Game {
    private Integer id;
    private LocalDateTime gameDate;
    private Player firstPlayer;
    private Player secondPlayer;
    private Integer playerFirstShotsCount;
    private Integer playerSecondShotsCount;
    private Long durationGame;

}

