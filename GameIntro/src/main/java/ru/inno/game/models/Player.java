package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Player {
    private Integer id;
    private String ip;
    private String nickname;
    private Integer points;
    private Integer winsCount;
    private Integer losesCount;
}

