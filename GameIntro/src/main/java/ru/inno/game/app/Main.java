package ru.inno.game.app;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.models.Game;
import ru.inno.game.repositories.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;
import ru.inno.game.sockets.GameServer;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("123");
        hikariConfig.setMaximumPoolSize(20);

        DataSource dataSource = new HikariDataSource(hikariConfig);

        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource) {
            @Override
            public Game getById(Long gameId) {
                return null;
            }
        };
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);

        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
       GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);

    }
}





