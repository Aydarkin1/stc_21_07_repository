public class Main {

    public void main(String[] args) {
        Figure[] figure = {new Figure(10, 24) {
            @Override
            public double getArea() {
                return 0;
            }

            @Override
            public String getName() {
                return null;
            }
        },
                new Triangle(10, 8, 10),
                new Circle(20),
                new Square(10, 10),
                new Ellipse(500, 400)};

        for (Figure fig : figure)
            System.out.println(fig.getName() + ": area = " + fig.getArea());
    }
}
//программа фигуры