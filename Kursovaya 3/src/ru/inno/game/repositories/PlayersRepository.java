package ru.inno.game.repositories;

import ru.inno.game.models.Player;

import java.util.Optional;

public interface PlayersRepository extends CrudRepository<Player> {

    Optional<Player> findPlayerByNickname(String name);
}
