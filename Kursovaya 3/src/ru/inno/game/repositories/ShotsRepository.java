package ru.inno.game.repositories;

import ru.inno.game.models.Shot;

public interface ShotsRepository extends CrudRepository<Shot> {

}
