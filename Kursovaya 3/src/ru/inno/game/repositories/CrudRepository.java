package ru.inno.game.repositories;

public interface CrudRepository<R> {
    void save(R r);

    void update(R r);
}
