package ru.inno.game.repositories;

import ru.inno.game.models.Player;
import ru.inno.game.util.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class PlayersRepositoryImpl implements PlayersRepository {
    private String fileName;
    private IdGenerator idGenerator;

    private Function<Player, String> playerToString = str ->
            str.getId() + "; " + str.getName() + "; " + str.getIpAddress() + "; " + str.getCountWins()
                    + "; " + str.getCountLose() + "; " + str.getPointsScored() + "\n";

    private Function<String, Player> playerParsing = line -> {
        String[] data = line.split("; ");

        return new Player(Long.parseLong(data[0]), data[1], data[2], Integer.parseInt(data[3]),
                Integer.parseInt(data[4]), Integer.parseInt(data[5]));
    };


    public PlayersRepositoryImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;

        try (Writer writer = new FileWriter(fileName, true)) {
            ;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void save(Player player) {
        try {
            player.setId(idGenerator.nextId());
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
                writer.write(player.getId() + "; " + player.getName() + "; " + player.getIpAddress()
                        + "; " + player.getCountWins() + "; " + player.getCountLose()
                        + "; " + player.getPointsScored() + "\n");
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void update(Player player) {
        try {
            List<Player> players = new ArrayList<>();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                String line = bufferedReader.readLine();
                while (line != null) {
                    Player playerUpdate = playerParsing.apply(line);
                    if (playerUpdate.getId().equals(player.getId())) {
                        playerUpdate.setName(player.getName());
                        playerUpdate.setIpAddress(player.getIpAddress());
                    }
                    players.add(playerUpdate);
                    line = bufferedReader.readLine();
                }
            }

            OutputStream outputStream = new FileOutputStream(fileName, false);
            for (Player playerUpdate : players) {
                outputStream.write(playerToString.apply(playerUpdate).getBytes());
            }
            outputStream.close();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public Optional<Player> findPlayerByNickname(String name) {
        try {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                String line = bufferedReader.readLine();
                while (line != null) {
                    Player playerName = playerParsing.apply(line);
                    if (playerName.getName().equals(name)) {
                        return Optional.of(playerName);
                    }
                    line = bufferedReader.readLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return Optional.empty();
    }
}
