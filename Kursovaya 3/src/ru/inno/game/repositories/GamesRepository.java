package ru.inno.game.repositories;

import ru.inno.game.models.Game;

public interface GamesRepository extends CrudRepository<Game> {
    Game findById(Long id);

}

