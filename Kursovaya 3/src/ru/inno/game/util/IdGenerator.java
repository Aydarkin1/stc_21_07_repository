package ru.inno.game.util;

public interface IdGenerator {
    Long nextId();
}

