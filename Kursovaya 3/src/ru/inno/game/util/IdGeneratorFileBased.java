package ru.inno.game.util;

import java.io.*;

public class IdGeneratorFileBased implements IdGenerator {
    private String fileName;


    public IdGeneratorFileBased(String fileName) {
        this.fileName = fileName;

        try (Writer writer = new FileWriter(fileName, true)) {
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public Long nextId() {
        try {
            Long Id;
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                String counter = bufferedReader.readLine();
                if (counter == null) counter = "0";
                Id = Long.parseLong(counter);
                Id++;
            }
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                writer.write(Long.toString(Id));
            }
            return Id;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
