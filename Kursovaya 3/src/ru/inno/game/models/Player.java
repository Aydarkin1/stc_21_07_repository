package ru.inno.game.models;

public class Player {
    private Long id;
    private String name;
    private String ipAddress;
    private Integer countWins;
    private Integer countLose;
    private Integer pointsScored;

    public Player(String name, String ipAddress, Integer countWins, Integer countLose, Integer pointsScored) {
        this.name = name;
        this.ipAddress = ipAddress;
        this.countWins = countWins;
        this.countLose = countLose;
        this.pointsScored = pointsScored;
    }

    public Player(Long id, String name, String ipAddress, Integer countWins, Integer countLose, Integer pointsScored) {
        this.id = id;
        this.name = name;
        this.ipAddress = ipAddress;
        this.countWins = countWins;
        this.countLose = countLose;
        this.pointsScored = pointsScored;
    }

    public Player(Long id, String name, String ipAddress) {
        this.id = id;
        this.name = name;
        this.ipAddress = ipAddress;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getCountWins() {
        return countWins;
    }

    public void setCountWins(Integer countWins) {
        this.countWins = countWins;
    }

    public Integer getCountLose() {
        return countLose;
    }

    public void setCountLose(Integer countLose) {
        this.countLose = countLose;
    }

    public Integer getPointsScored() {
        return pointsScored;
    }

    public void setPointsScored(Integer pointsScored) {
        this.pointsScored = pointsScored;
    }
}