package ru.inno.game.models;

public class Shot {
    private Long shotId;
    private String shotTime;
    private boolean isHit;
    private Game game;
    private Player shooter;
    private Player target;
}