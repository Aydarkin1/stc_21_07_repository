package ru.inno.game.services;


public interface PlayerService {
    void creat(String name, String ipAddress, Integer countWins, Integer countLose, Integer pointsScored);

    void update(Long playerId, String name, String ipAddress);

    boolean findPlayerByNickname(String name);


}
