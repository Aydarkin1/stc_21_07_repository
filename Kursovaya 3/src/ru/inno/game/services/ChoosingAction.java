package ru.inno.game.services;

import java.util.Scanner;

public class ChoosingAction {

    public void choosing(Scanner scan, PlayerService playerService) {
        while (true) {
            System.out.println("1. Добовление игрока");
            System.out.println("2. Обновление игрока");
            System.out.println("3. Поиск по имени игрока");

            int command = scan.nextInt();
            scan.nextLine();

            switch (command) {
                case 1: {
                    System.out.println("Вввидите имя игрока");
                    String name = scan.nextLine();
                    System.out.println("Вввидите ip address игрока");
                    String ipAddress = scan.nextLine();
                    System.out.println("Вввидите кол-во выигрышей игрока");
                    Integer countWins = Integer.parseInt(scan.nextLine());
                    System.out.println("Вввидите кол-во проигрышей игрока");
                    Integer countLose = Integer.parseInt(scan.nextLine());
                    System.out.println("Вввидите счет игрока");
                    Integer pointsScored = Integer.parseInt(scan.nextLine());
                    playerService.creat(name, ipAddress, countWins, countLose, pointsScored);
                    System.err.println("\nДанные занесены\n");
                    break;
                }
                case 2: {
                    System.out.println("Вввидите id игрока");
                    Long playerId = Long.valueOf(scan.nextLine());
                    System.out.println("Вввидите новое имя игрока");
                    String namePlayer = scan.nextLine();
                    System.out.println("Вввидите новый ip address игрока");
                    String ipAddress = scan.nextLine();
                    playerService.update(playerId, namePlayer, ipAddress);
                    System.err.println("\nДанные изменены\n");
                    break;
                }
                case 3:
                    System.out.println("Вввидите имя игрока");
                    String name = scan.nextLine();
                    playerService.findPlayerByNickname(name);
                    break;
            }
        }
    }
}
