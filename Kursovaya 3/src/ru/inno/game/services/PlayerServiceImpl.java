package ru.inno.game.services;

import ru.inno.game.models.Player;
import ru.inno.game.repositories.PlayersRepository;

import java.util.Optional;


public class PlayerServiceImpl implements PlayerService {

    private PlayersRepository playersRepository;

    public PlayerServiceImpl(PlayersRepository playersRepository) {
        this.playersRepository = playersRepository;
    }

    @Override
    public void creat(String name, String ipAddress, Integer countWins, Integer countLose, Integer pointsScored) {
        Player player = new Player(name, ipAddress, countWins, countLose, pointsScored);
        playersRepository.save(player);


    }

    @Override
    public void update(Long playerId, String name, String ipAddress) {
        Player player = new Player(playerId, name, ipAddress);
        playersRepository.update(player);

    }

    @Override
    public boolean findPlayerByNickname(String name) {
        Optional<Player> playerOptional = playersRepository.findPlayerByNickname(name);
        if (playerOptional.isPresent()) {
            Player player = playerOptional.get();
            System.err.println("\nРезульт: " + "id игрока: " + player.getId() + "; "
                    + "Имя игрока: " + player.getName() + "\n");
            return true;
        } else {
            System.err.println("\nНет такого игрока\n");
            return false;
        }
    }
}
