package ru.inno.game;

import ru.inno.game.repositories.PlayersRepository;
import ru.inno.game.repositories.PlayersRepositoryImpl;
import ru.inno.game.services.ChoosingAction;
import ru.inno.game.services.PlayerService;
import ru.inno.game.services.PlayerServiceImpl;
import ru.inno.game.util.IdGeneratorFileBased;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);

        PlayersRepository playersRepository = new PlayersRepositoryImpl("players.txt",
                new IdGeneratorFileBased("players_id.txt"));

        PlayerService playerService = new PlayerServiceImpl(playersRepository);

        ChoosingAction choosingActions = new ChoosingAction();

        choosingActions.choosing(scan, playerService);
    }

}
