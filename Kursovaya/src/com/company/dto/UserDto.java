package com.company.dto;

import com.company.models.User;

import java.util.ArrayList;
import java.util.List;

public class UserDto {
    private String ipAdress;
    private String name;
    private String id;
    public UserDto(String ipAdress){
        this.ipAdress = ipAdress;
        this.name = name;
        this.id = id;

    }


    public static UserDto from(User user) {
        return new UserDto(user.getIpAdress());

    }
    public static List<UserDto> from(List<User> users){
        List<UserDto>result = new ArrayList<>();
        for(User user: users){
            result.add(from (user));
        }
        return result;
    }

    public String getIpAdress() {
        return ipAdress;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
