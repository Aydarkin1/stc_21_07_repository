package com.company.models;

public class Shoot {

    private int scored;// попадание
    private int missed;// пропущено
    private Game firstGame; // первая игра
    private Game secondGame;// вторая игра
    private Game thirdGame;// третья игра

    public Shoot(int scored, int missed) {
        this.scored = scored;
        this.missed = missed;
    }

    public Shoot(Game firstGame, Game secondGame, Game thirdGame) {
        this.firstGame = firstGame;
        this.secondGame = secondGame;
        this.thirdGame = thirdGame;
    }

    // возвращает количесво промахов
    int gamesMiss() {
        int gamesMiss = 0;
        if (firstGame.isMiss()) {
            gamesMiss++;
        }
        if (secondGame.isMiss()) {
            gamesMiss++;
        }
        if (thirdGame.isMiss()) {
            gamesMiss++;
        }
        return gamesMiss;
    }

    // возвращает количество попаданий
    int gamesGot() {
        int gamesGot = 0;
        if (firstGame.isGot()) {
            gamesGot++;
        }
        if (secondGame.isGot()) {
            gamesGot++;
        }
        if (thirdGame.isGot()) {
            gamesGot++;
        }
        return gamesGot;
    }

    // количество набранных очков
    int scores() {
        int scores = 0;
        scores = firstGame.getPointsScored() +
                secondGame.getPointsScored() +
                thirdGame.getPointsScored();
        return scores;
    }

    public String gameGot() {
    }
}







