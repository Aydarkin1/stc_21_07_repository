package com.company.models;

public class Game {

    private Object players;
    private Object date;

    public Game(int date, int players) {
        this.date = date;
        this.players = players;

    }

    public String getDate() {
        return (String) date;
    }

    public String getPlayers() {
        return (String) players;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        Game game = (Game) object;
        return java.util.Objects.equals(date, game.date) && java.util.Objects.equals(players, game.players);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(super.hashCode(), date, players);
    }


    public void add(Game game) {
    }

    public boolean isMiss() {
    }

    public boolean isGot() {
    }

    public int getPointsScored() {
    }

    public static void main(String[] args) {
        Game firstGame = new Game(28, 2);
        Game secondGame = new Game(25, 2);
        Game thirdGame = new Game(23, 2);

        Shoot shoot = new Shoot(firstGame, secondGame, thirdGame);
        System.out.println("Количество игр выигранных игроками:" + shoot.gameGot());
        System.out.println("Количество игр проигранных игроками:" + shoot.gamesMiss());
        System.out.println("Общее количество очков игроков" + shoot.scores());
    }

}





