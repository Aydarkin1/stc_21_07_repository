package com.company.models;

public class User {
    private String ipAdress;
    private String name;
    private String id;

    public User(String ipAdress, String name, String id) {
        this.ipAdress = ipAdress;
        this.id = id;
        this.name = name;

    }

    public String getIpAdress() {
        return ipAdress;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        User user = (User) object;
        return java.util.Objects.equals(ipAdress, user.ipAdress) && java.util.Objects.equals(name, user.name) && java.util.Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(super.hashCode(), ipAdress, name, id);
    }


    public void add(User user) {
    }
}
