package com.company;

import com.company.dto.UserDto;
import com.company.repositories.UserRepositoryFileBasedImpl;
import com.company.repositories.UsersRepository;
import com.company.services.UsersService;
import com.company.services.UsersServiceImpl;

import java.util.List;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        UsersRepository usersRepository = new UserRepositoryFileBasedImpl(fileName:"Users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository);

        boolean isAuthenticated = false;

        while (true) {
            System.out.println("1. Регистрация");
            System.out.println("2. Авторизация");
            System.out.println("3. Получить список пользователей");

            int command = scanner.nextInt();
            scanner.nextLine();
            switch (command) {
                case 1: {
                    String ipAdress = scanner.nextLine();
                    String name = scanner.nextLine();
                    String id = scanner.nextLine();
                    usersService.signUp(ipAdress, name, id);
                    break;
                }
                case 2: {
                    String ipAdress = scanner.nextLine();
                    String name = scanner.nextLine();
                    String id = scanner.nextLine();
                    isAuthenticated = usersService.signIn(ipAdress, name, id);
                    break;
                }
                case 3:
                    if (isAuthenticated) {
                        List<UserDto> users = usersService.getUsers();
                        for (UserDto user : users) {
                            System.out.println(user.getIpAdress());
                            System.out.println(user.getName());
                            System.out.println(user.getId());
                        }
                    } else {
                        System.err.println("Вы не прошли аутентификацию");
                    }
                    break;
            }
        }
    }
}