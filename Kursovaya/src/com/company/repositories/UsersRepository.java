package com.company.repositories;

import com.company.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {
    void save(User user);
    Optional <User> findByIpAdress(String ipAdress);
    Optional<User> findById(String id);


    List<User> findAll();
}
