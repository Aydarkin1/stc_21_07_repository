package com.company.services;

import com.company.dto.UserDto;
import com.company.models.User;
import com.company.repositories.UsersRepository;

import java.util.List;
import java.util.Optional;

import static com.company.dto.UserDto.from;

public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;
    private String ipAdress;
    private String name;
    private String id;

    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void signUp(String ipAdress, String name, String id) {

        User user = new User(ipAdress, name, id);
        usersRepository.save(user);
    }

    @Override
    public boolean signIn(String ipAdress, String name, String id) {
        Optional<User> userOptional = usersRepository.findByIpAdress(ipAdress);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            userOptional = usersRepository.findById(id);
            if (userOptional.isPresent()) {
                user = userOptional.get();
                if (user.getName().equals(name)) {
                    return true;
                } else {
                    System.err.println("Пользователь не прошел проверку по ipAdress/name");
                }
            }
            System.err.println("Пользователь не прошел проверку по id/name");
        }
        System.err.println("Пользователь не прошел проверку по ipAdress/name");
        return false;
    }

    @Override
    public List<UserDto>getUsers(){
        List<User> users = usersRepository.findAll();
        return from(users);
    }
}
