package com.company.services;

import com.company.dto.UserDto;

import java.util.List;

public interface UsersService {
    void signUp(String ipAdress, String name, String id);

    boolean signIn(String ipAdress, String name, String id);

    List<UserDto> getUsers();
}
