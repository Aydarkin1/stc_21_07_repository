-создание таблицы с пользователями
drop table if exists account;
create table account
(
    id         serial primary key,
    ip char(20) ,
    first_name char(20) ,
    max_ochkov integer default 1 check (max_ochkov>0),
    pobeda     integer
);
insert into account(ip,first_name,max_ochkov,pobeda) values('192.168.1.1','Marsel',100,100);
insert into account(ip,first_name, max_ochkov, pobeda) values('192.168.1.2','Viktor',100,100);



drop  table if exists game ;
create table game(
                     date integer not null,
                     vistrel_value integer,
                     time_value integer,
                     owner_id integer,
                     foreign key (owner_id) references account (id)

);

insert into game(date,vistrel_value,time_value) values(25-05-2021,100,10-00-00);
insert into game(date,vistrel_value,time_value) values(26-05-2021,100,10-00-00);

drop table if exists vistrel;
create table vistrel(
                        time_value integer,
                        popal_nepopal char(20),
                        game integer,
                        vistrel_player  serial,
                        owner_id integer,
                        foreign key (owner_id) references account (id)
);
insert into vistrel(time_value, popal_nepopal,game) values(10-00-00,'popal',1);
insert into vistrel(time_value, popal_nepopal,game) values(12-00-00,'popal',2);

select*from account;

