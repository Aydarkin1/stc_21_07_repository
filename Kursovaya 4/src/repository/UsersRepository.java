package repository;

import model.User;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<User>{

    Optional<User> findFirstByIpAdressDesc(String ipAdress);




}
