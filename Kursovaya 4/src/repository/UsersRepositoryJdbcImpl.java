package repository;

import model.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcImpl implements UsersRepository {
    private static final String SQL_SELECT_ALL = "select*from account";

    private DataSource dataSource;

    public UsersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(User model) {

    }

    @Override
    public Optional<User> findById(Integer id) {
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
             try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(SQL_SELECT_ALL)) {
            while (result.next()) {
                Integer id = result.getInt("id");
                String ipAdress = result.getString("ipAdress");
                String firstName = result.getString("first_name");
                Integer max_ochkov = result.getInt("max_ochkov");
                Integer pobeda = result.getInt("pobeda");
                User user = new User(id, ipAdress, firstName, max_ochkov, pobeda);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return users;
    }


    @Override
    public Optional<User> findFirstByIpAdressDesc(String ipAdress) {
        return Optional.empty();
    }
}
