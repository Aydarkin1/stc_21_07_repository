package repository;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    void save(T model);
  Optional <T> findById(Integer id);
    List<T> findAll();
}

