import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import repository.UsersRepository;
import repository.UsersRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main {
    public static void main(String[] args) throws Exception {
       // DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/postgres", "postgres", "123");
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("123");
        hikariConfig.setMaximumPoolSize(20);

        DataSource dataSource= new HikariDataSource(hikariConfig);

        ExecutorService service = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 1; i++){
            service.submit(() -> {
                UsersRepository usersRepository = new UsersRepositoryJdbcImpl(dataSource);
                for (int j = 0; j < 10; j++) {
                    System.out.println(usersRepository.findAll().size());
                }
            });
        }
    }
}


