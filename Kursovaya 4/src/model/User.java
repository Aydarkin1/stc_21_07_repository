package model;
public class User {
    private Integer id;
    private String ipAdress;
    private String firstName;
    private Integer max_ochkov;
    private Integer pobeda;

    public User(String ipAdress) {
        this.ipAdress = ipAdress;
    }

    public User(Integer id, String ipAdress, String firstName, Integer max_ochkov, Integer pobeda) {
    }

    public String getIpAdress() {
        return ipAdress;
    }

    public User(Integer id, String firstName, Integer max_ochkov, Integer pobeda) {
        this.id = id;
        this.firstName = firstName;
        this.max_ochkov = max_ochkov;
        this.pobeda = pobeda;
    }

    public User(String firstName, Integer max_ochkov, Integer pobeda) {
        this.firstName = firstName;
        this.max_ochkov = max_ochkov;
        this.pobeda = pobeda;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Integer getMax_ochkov() {
        return max_ochkov;
    }

    public Integer getPobeda() {
        return pobeda;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", ipAdress='" + ipAdress + '\'' +
                ", firstName='" + firstName + '\'' +
                ", max_ochkov=" + max_ochkov +
                ", pobeda=" + pobeda +
                '}';
    }
}
