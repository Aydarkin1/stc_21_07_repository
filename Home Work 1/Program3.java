import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int c = scanner.nextInt();
        int myArray[] = new int[c];
        int p = 1;
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = scanner.nextInt();
        }
        for (int j = 0; j < myArray.length; j++) {
            if (primeNumber(myArray[j]) == 1) {
                p = p * myArray[j];
                System.out.println(p);
            }
        }

    }

    private static int primeNumber(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return 0;
            }
            if ((i == number) || (i > Math.sqrt(number))) {
                return 1;
            }
        }
        return 0;
    }
}
//моя программа
