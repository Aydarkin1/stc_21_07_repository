package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        double average_out;
        int array_to_number;

        Scanner scan = new Scanner(System.in);
        System.out.print("Размерность массива : ");

        int array_size = scan.nextInt();
        int array_values[] = new int[array_size];
        System.out.println("Элементы масива : ");

        for (int i = 0; i < array_size; i++) {
            array_values[i] = scan.nextInt();
        }

        int tmp_swap_array[] = clone(array_values);
        int tmp_array_to_number[] = clone(array_values);

        System.out.println("Исходный массив : " + Arrays.toString(array_values));

        print_array_sum(array_values);

        print_inversion_arr(array_values);
        System.out.println("Инверсия массива : " + Arrays.toString(array_values));

        average_out = average_of_array(array_values);
        System.out.println("Среднее арифметическое элементов массива : " + average_out);


        swap_array(tmp_swap_array);
        System.out.println("Массив c свопом max и min значения : " + Arrays.toString(tmp_swap_array));

        bubble_sort(array_values);
        System.out.println("Сортировка массива методом пузырька : " + Arrays.toString(array_values));


        array_to_number = array_to_number(tmp_array_to_number);
        System.out.println("Преобразование массива в число : " + array_to_number);
    }


    public static void print_array_sum(int array[]) {
        int n = array.length;
        int sum = 0;

        for (int i = 0; i < n; i++) {
            sum += array[i];
        }

        System.out.println("Сумму элементов массива : " + sum);
    }

    public static int[] print_inversion_arr(int array[]) {
        int tmp;

        for (int i = 0; i < array.length / 2; i++) {

            tmp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = tmp;

        }
        return array;
    }

    public static double average_of_array(int array[]) {
        int n = array.length;
        int sum = 0;
        double average;

        for (int i = 0; i < n; i++) {
            sum += array[i];
        }
        average = (double) sum / array.length;
        return average;
    }

    public static int[] swap_array(int[] array) {
        int max = array[0];
        int min = array[0];
        int idx_max = 0;
        int idx_min = 0;
        int temp;

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                idx_max = i;
            }

            if (array[i] < min) {
                min = array[i];
                idx_min = i;
            }
        }

        temp = array[idx_max];
        array[idx_max] = array[idx_min];
        array[idx_min] = temp;

        return array;
    }

    public static int[] bubble_sort(int array[]) {
        int last_item = array.length - 1;
        int temp;

        for (int i = 0; i < last_item; i++)
            for (int j = 0; j < last_item - i; j++)
                if (array[j] > array[j + 1]) {

                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
        return array;
    }

    public static int array_to_number(int array[]) {
        String str = "";
        int n;

        for (int i = 0; i < array.length; i++) {
            str += array[i];

        }
        n = Integer.parseInt(str);
        return n;
    }

    private static int[] clone(int[] source) {
        return source.clone();
    }

}
