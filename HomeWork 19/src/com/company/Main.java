package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        ArrayList<Automobile> automobiles = new ArrayList<>();
        Stream<Automobile> automobileStream = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
            reader.lines()
                    .forEach(System.out::println);

            automobileStream = automobiles.stream();

            automobileStream
                    .filter(automobile -> automobile.getColor().equals("black") || automobile.getProbeg() == 0)
                    .map(automobile -> automobile.getColor() + " " + automobile.getProbeg())
                    .forEach(automobile -> System.out.println("Номера всех автомобилей имеющих черный цвет" + "цвет  или нулевой пробег.=" + automobile.getNumber));

            Long countUniqueAutomobile = automobileStream
                    .filter(automobile -> (automobile.getPrice() <= 800000) && (automobile.getPrice() >= 700000))
                    .map(automobile -> automobile.getModel()).distinct().count();
            System.out.println("\nКоличество уникальных моделей в ценовом диапазоне от 700 до 800тыс.=" + countUniqueAutomobile);

            String colorAutomobile = automobileStream
                    .min(Comparator.comparingLong(Automobile::getPrice)).get().getColor();
            System.out.println("\nСтоимость автомобиля с минимальной стоимостью.=" + colorAutomobile);


            Double averangePrice = automobileStream
                    .filter(automobile -> automobile.getModel().equals("Toyota Camry"))
                    .mapToLong(automobile -> (long) automobile.getPrice()).average().getAsDouble();
            System.out.println("\nСредняя стоимость Toyota Camry.=" + averangePrice);

        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        } finally {
            automobileStream.close();
        }
    }
}












