package com.company;

public class Automobile {
    private String number;
    private String model;
    private String color;
    private double probeg;
    private double price;

    public Automobile(String number, String model, String color, Long probeg, Long price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.probeg = probeg;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public double getProbeg() {
        return probeg;
    }

    public double getPrice() {
        return price;
    }

}


